from django.apps import AppConfig


class ScrappyappConfig(AppConfig):
    name = 'scrappyapp'
