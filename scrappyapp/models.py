from django.db import models
import datetime

# Create your models here.

class Yelp(models.Model):
    name=models.CharField(max_length=50)
    number=models.CharField(max_length=20)
    address=models.CharField(max_length=500)
    date=models.DateTimeField(default=datetime.datetime.now())
    visit_site=models.CharField(max_length=100)
    city=models.CharField(max_length=50)
    state=models.CharField(max_length=20)
    zip = models.CharField(max_length=20)
    country = models.CharField(max_length=20)
    search_term = models.CharField(max_length=50)
    email = models.CharField(max_length=60)
